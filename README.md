# Ansible Role for monitoring

## Client
### Telegraf
This role installs Telegraf agent on node to push system monitoring on the server

## Server
### Docker
All monitoring component are running into a Docker container. We first install docker to install other components

### Influxdb
All Telegraf metrics are stored in an InfluxDB database

### Grafana
We use Grafana to display information gathered in InlufxDB database